ARG branch
FROM  ubuntu:18.04
CMD ["/sbin/my_init"]
RUN apt-get update && apt-get upgrade -y
RUN apt-get install sudo curl -y
RUN sudo apt install libgpgme-dev libassuan-dev btrfs-progs libdevmapper-dev libostree-dev -y
RUN sudo apt-get install git -y
RUN curl -O https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
RUN tar -xvf go1.8.linux-amd64.tar.gz
RUN mv go/bin/go /usr/bin/
RUN mv go /usr/local
ENV GOPATH /home/bwfla/go
ENV PATH /home/bwfla/go/bin:$PATH
RUN go get github.com/LK4D4/vndr
RUN git clone --branch v0.1.31 https://github.com/projectatomic/skopeo $GOPATH/src/github.com/projectatomic/skopeo
RUN cd $GOPATH/src/github.com/projectatomic/skopeo && make binary-local && mv $GOPATH/src/github.com/projectatomic/skopeo/skopeo /usr/bin/
RUN rm /go1.8.linux-amd64.tar.gz && rm -rf /home/bwfla/go/
